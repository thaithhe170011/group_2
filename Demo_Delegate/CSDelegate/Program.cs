﻿namespace CSDelegate
{
    internal class Program
    {
        public delegate void TinhToan(int a, int b);
        
    public static void Add(int a, int b)
    {
        Console.WriteLine("Tong = " + (a + b));
    }
    public static void Sub(int a, int b)
    {
        Console.WriteLine("Hieu = " + (a - b));
    }
    static void Main(string[] args)
    {

        Console.WriteLine("Hello, World!");
        Action<int, int> action = Add;
        action?.Invoke(1, 2);
        TinhToan tinhToan = Add;
        tinhToan += Sub;
        tinhToan?.Invoke(5, 2);
        tinhToan = (a, b) => { Console.WriteLine($"{a} va {b}"); };
        tinhToan?.Invoke(9,10);






























    }
}
    }

